package com.punkapi;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.resource.TestBase;
import org.testng.annotations.Test;
import org.testng.Assert;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import static io.restassured.RestAssured.given;

public class BeersTestCase {
    JsonPath js;

    @Test(groups = {"Regression","Smoke"})
    public void VerifyAllBeers() {
        RestAssured.baseURI= TestBase.URIDetails();
        ValidatableResponse response=given().when().get("/v2/beers").then();
        response.assertThat().statusCode(200).header("Content-Type", "application/json; charset=utf-8")
                .header("Connection", "keep-alive");
        int arraysize = response.extract().jsonPath().getList("$").size();
        List<Object> list=response.extract().jsonPath().getList("$");
        Map<Integer, String> map = new HashMap<>();
        for(Object ele:list)
       {

       }

        System.out.println("Total no of records as JSON array is "+arraysize);
        Assert.assertEquals(arraysize, 25);

    }
    @Test(groups = {"Regression"})
    public void verifyBrewBeforeDate()
    {
        RestAssured.baseURI= TestBase.URIDetails();
       List<Object> brewslist= given().queryParam("brewed_before","02/2009").when().get("/v2/beers").as(List.class);
       System.out.println(brewslist.size());
       Map<String,Object> emp1= (Map<String, Object>) brewslist.get(0);
       Assert.assertNotNull(emp1.get("id"));
       Assert.assertNotNull(emp1.get("name"));
       Assert.assertNotNull(emp1.get("description,"));
       Assert.assertNotNull(emp1.get("abv"));


    }

    @Test(groups = {"Regression"})
    public void verifyBeersWithAbvGreaterThanSix()
    {
        RestAssured.baseURI= TestBase.URIDetails();///v2/beers?abv_gt=6
        Response res=given().queryParam("abv_gt","6").when().get("/v2/beers?abv_gt=6");
        res.then().assertThat().statusCode(200)
               .header("Content-Type","application/json; charset=utf-8").header("server","cloudflare");


    }

    @Test(groups = {"Regression"})
    public void verifyPegination()
    {
        RestAssured.baseURI="https://api.punkapi.com";
        Response res=given().queryParam("page","2").queryParam("per_page","5").when().get("/v2/beers");
        String beers= res.asString();
        js=new JsonPath(beers);
        int noofbeers=js.getInt("$.size()");//If no array name then use $
        Assert.assertEquals(noofbeers,5);
        System.out.println("No of beers returned is "+noofbeers +" Hence assertion is passed");
        System.out.println(beers);

    }
}
