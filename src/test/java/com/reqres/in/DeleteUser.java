package com.reqres.in;

import org.testng.annotations.Test;
import org.testng.Assert;
import org.testng.annotations.Test;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.*;
import java.net.StandardSocketOptions;
import java.util.List;
import java.util.Map;
import java.util.jar.JarEntry;
import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.requestSpecification;

public class DeleteUser {
    @Test(groups = "Smoke")
    public void deleteUser()
    {
        RestAssured.baseURI="https://reqres.in";
        given().when().delete("/ api/users/2").then().assertThat().statusCode(200);
    }
}
