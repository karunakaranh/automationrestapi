package com.reqres.in;
import org.testng.annotations.Test;
import org.testng.Assert;
import org.testng.annotations.Test;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.*;
import java.net.StandardSocketOptions;
import java.util.List;
import java.util.Map;
import java.util.jar.JarEntry;
import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.requestSpecification;

public class GetAllUsers {
JsonPath js;
    @Test(groups = {"Smoke","Regression"})
    public void verifyAllUser()
    {
        RestAssured.baseURI="https://reqres.in";
       Response response=given().get("/api/users?page=2");
       response.then().assertThat().statusCode(200).contentType("application/json; charset=utf-8").extract().asString();
        js=new JsonPath(response.asString());
        System.out.println(js.getList("data"));
        System.out.println("getUserdata as JSON object is "+response.jsonPath().getList("data").get(0));
        String email=response.jsonPath().getMap("data[0]").get("email").toString();
        System.out.println("1st User email is is " +email);
        Assert.assertNotNull(email);
        Assert.assertEquals(js.getInt("page"),2);
        Assert.assertEquals(js.getInt("per_page"),6);
        Assert.assertEquals(js.getInt("total"),12);
        Assert.assertEquals(js.getInt("total_pages"),2);
        int arraysize=js.getInt("data.size()");
        System.out.println(arraysize );
        Assert.assertEquals(arraysize,6);
    }
    @Test(groups = {"Smoke","Regression"})
    public void verifyUserDetails()
    {
        RestAssured.baseURI="https://reqres.in";
       String userdetails= given().get("/api/users/2").then().assertThat().
                statusCode(200).header("Content-Type","application/json; charset=utf-8").extract().asString();
       js=new JsonPath(userdetails);
      String id=js.getString("data.id");//path of the json
      Assert.assertEquals(id,"2");

        System.out.println("id value is "+id+" Hence assertion is passed");


    }
    @Test(groups = {"Regression"})
    public void verifyUserNotFound()
    {
        RestAssured.baseURI="https://reqres.in";
        given().get("/api/users/23").then().assertThat().statusCode(404)
                .header("Server","cloudflare");
    }


}
