package com.reqres.in;


import io.restassured.specification.ResponseSpecification;
import org.testng.annotations.Test;
import org.testng.Assert;
import org.testng.annotations.Test;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.*;
import java.net.StandardSocketOptions;
import java.util.List;
import java.util.Map;
import java.util.jar.JarEntry;
import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.requestSpecification;

public class SuccessfulLogin {

    @Test(groups = "smoke")
    public void verifySuccessfulLogin()
    {
        RestAssured.baseURI="https://reqres.in";
        String user=given().body("{\n" +
                "    \"email\": \"eve.holt@reqres.in\",\n" +
                "    \"password\": \"cityslicka\"\n" +
                "}").
                when().post("/api/login")
        .then().assertThat().statusCode(400).extract().response().asString();
        JsonPath js=new JsonPath(user);
        String token=js.getString("token");
        System.out.println(token);
        Assert.assertEquals(token,"QpwL5tke4Pnpja7X4");

    }
}
