package com.reqres.in;

import com.resource.UserInfo;
import org.testng.annotations.Test;
import org.testng.annotations.Test;
import org.testng.Assert;
import org.testng.annotations.Test;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.*;
import java.util.List;
import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.trustStore;

public class CreateUser {
 UserInfo uinfo;
    @Test(groups = "Regression")
    public void verifyUserCreation()
    {

        uinfo=new UserInfo();
        uinfo.setName("Karan");
        uinfo.setJob("SDET");

        RestAssured.baseURI="https://reqres.in";
         String response=given().log().all().header("Content-Type","application/json").body(uinfo)
            .when().post("/api/users").then().assertThat().statusCode(201)
            .header("Connection","keep-alive")
            .extract().response().asString();
            JsonPath js=new JsonPath(response);
            String username=js.getString("name");
            Assert.assertEquals(username,uinfo.getName());
            System.out.println("User name is "+username+" And Assertion is passed");
           String jobTitle=js.getString("job");
           Assert.assertEquals(jobTitle,uinfo.getJob());
           System.out.println("JOb title is "+jobTitle+" And Assertion is passed");
    }
}
